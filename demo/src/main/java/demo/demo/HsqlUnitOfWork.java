package demo.demo;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Entity;
public class HsqlUnitOfWork implements UnitOfWork
{

	private Map<Entity,UnitOfWorkRepository> entities;
	public HsqlUnitOfWork() 
	{
		setEntities(new HashMap<Entity,UnitOfWorkRepository>());

}
	public Map<Entity,UnitOfWorkRepository> getEntities() 
	{
		return entities;
	}
	public void setEntities(Map<Entity,UnitOfWorkRepository> entities)
	{
		this.entities = entities;
	}
	public void saveChanges() 
	{
		
	}
	public void undo()
	{
	
	}
	public void markAsNew(Entity entity, UnitOfWorkRepository repo)
			{
		
	}
	public void markAsDeleted(Entity entity, UnitOfWorkRepository repo) 
	{
		
		
	}
	public void markAsChanged(Entity entity, UnitOfWorkRepository repo)
	{
		
		
	}
	public void Commit() 
	{
		
		
	}
}