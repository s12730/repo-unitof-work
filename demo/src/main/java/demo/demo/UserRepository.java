package demo.demo;

public interface UserRepository extends Repository<Object>
{

	void withLogin (String login);
	void withLoginAndPassword(String login, String password);
	void setupPermisions(User user);
}
