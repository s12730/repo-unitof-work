package demo.demo;



public interface UnitOfWorkRepository 
{
	  void persistAdd(Entity entity);
	  void persistDelete(Entity entity);
	  void persistUpdate(Entity entity);
	 
}
