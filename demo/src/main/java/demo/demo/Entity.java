package demo.demo;

import java.lang.Thread.State;

public abstract class Entity 
{
private int id;
private State EntityState;
public int getId() 
{
	return id;
}

public void setId(int id) 
{
	this.id = id;
}

public State getEntityState()
{
	return EntityState;
}

public void setEntityState(State entityState) 
{
	EntityState = entityState;
}

}
