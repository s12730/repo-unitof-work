package demo.demo;


public interface EnumerationValueRepository extends Repository<Object>
{
void withName (String name);
void withIntKey(int key, String name);
void withStringKey(String key, String name);
}
