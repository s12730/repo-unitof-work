package demo.demo;

public class PagingInfo 
{
	private int pageSize;
	private int page;
	private int totalCount;
	public int getPage() 
	{
		return page;
	}
	public void setPage(int page) 
	{
		this.page = page;
	}
	public int getPageSize() 
	{
		return pageSize;
	}
	public void setPageSize(int pageSize) 
	{
		this.pageSize = pageSize;
	}
	public int getTotalCount()
	{
		return totalCount;
	}
	public void setTotalCount(int totalCount) 
	{
		this.totalCount = totalCount;
	}

}
