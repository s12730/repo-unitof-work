package demo.demo;


public enum EntityState 
{
	
	NEW, MODIFIED, UNCHANGED, DELETED, UNKNOWN;

}
