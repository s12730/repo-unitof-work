package demo.demo;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Entity;


public interface UnitOfWork 
{
	
	Map<Entity,UnitOfWorkRepository> entities=new HashMap<Entity,UnitOfWorkRepository>();


public void saveChanges();


public void undo();


void markAsNew(Entity entity,UnitOfWorkRepository repo);
void markAsDeleted(Entity entity,UnitOfWorkRepository repo);
void markAsChanged(Entity entity,UnitOfWorkRepository repo);
void Commit();

}